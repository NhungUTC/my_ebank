/**
 * @format
 */

import {AppRegistry} from 'react-native';
// import App from './App';
import {name as appName} from './app.json';
import MainNavigation from './src/routes/main-naviagtion';

// import { MainNavigationComponent } from './src/routes/main-naviagtion';

AppRegistry.registerComponent(appName, () => MainNavigation);
