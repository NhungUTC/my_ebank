import 'react-native-gesture-handler';
import * as React from 'react';
import { Image, Text, View, StyleSheet } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { LoginScreen } from '../features/login/login-screen';

import * as screenName from './screen-name'

import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import images from '../res/images';

import colors from '../res/colors';
// import { HomeScreen } from '../features/home/home.screen';
import { TransfersScreen } from '../features/transfers/transfers.screen';
// import { SettingScreen } from '../features/setting/setting.screen';
import { HomeScreen } from '../features/home/home.screen';
// import { LoginMapScreen } from '../features/login-map/login-map.screen';
export const BottomTabMain = createMaterialBottomTabNavigator(
  {
    //1
    [screenName.HomeScreen]: {
      screen: HomeScreen,
      navigationOptions: ({ navigation }) => ({
        title: "",
        tabBarLabel:"Trang chủ",
        tabBarIcon: ({ focused }) => {
          const img = focused ? images.ic_homeActive : images.ic_home
          return (
            <View style={styles.viewIcon}>
              <Image style={styles.icon} source={img}></Image>
            </View>
          )
        },
        // barStyle: { backgroundColor: colors.primaryColor },  
      }),
    },
    //2
    [screenName.TransfersScreen]: {
      screen: TransfersScreen,
      navigationOptions: ({ navigation }) => ({
        title: "Chuyển tiền",
        tabBarIcon: ({ focused }) => {
          const img = focused ? images.ic_transfersActive : images.ic_transfers
          return (
            <View style={styles.viewIcon}>
              <Image style={styles.icon} source={img}></Image>
            </View>
          )
        },
      }),
    },
    //3
    [screenName.LoginScreen]: {
      screen: LoginScreen,
      navigationOptions: ({ navigation }) => ({
        title: "Cài đặt",
        tabBarIcon: ({ focused }) => {
          const img = focused ? images.ic_settingActive : images.ic_setting
          return (
            <View style={styles.viewIcon}>
              <Image style={styles.icon} source={img}></Image>
            </View>
          )
        },
      }),
    },
  },
  //init
  {
    initialRouteName: screenName.HomeScreen,
    activeColor: colors.primaryColor,
    inactiveColor:"#99A0AD",
    barStyleLight: {
      height: 56,
      backgroundColor: colors.white,

    },
  }
);

const styles = StyleSheet.create({
  icon: {
    width: 24,
    height: 24
  },
  viewIcon: {
    width: 40,
    height: 40,
    // marginBottom:20,
    // backgroundColor: 'pink',
    alignItems: 'center',
    justifyContent: 'center',
    paddingBottom:15
  
  
  },
  barBottom: {

  }
})
