// import 'react-native-gesture-handler';
// import * as React from 'react';
// import { Text, View } from 'react-native';
// import { createStackNavigator } from '@react-navigation/stack';
// import { NavigationContainer } from '@react-navigation/native';
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
// import { LoginScreen } from '../features/login/login-screen';
// import { TabNavigatorComponent } from './tab-navigator';



// const Tab = createBottomTabNavigator();
// const Stack = createStackNavigator();
// export const MainNavigationComponent=()=>{
//   return (
//     <NavigationContainer>
//       <Stack.Navigator
//       screenOptions={{
//         headerShown: false
//       }}
//       >
//             <Stack.Screen name="Tab" component={TabNavigatorComponent  }  />
//         {/* <Stack.Screen name="Home" component={LoginScreen} /> */}
      
//       </Stack.Navigator>
     
//     </NavigationContainer>
//   );
// }

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { HomeScreen } from '../features/home/home.screen';
import { LoginMapScreen } from '../features/login-map/login-map.screen';
import { LoginPhoneScreen } from '../features/login-phone/login-phone.screen';
import { LoginScreen } from '../features/login/login-screen';
import { SettingScreen } from '../features/setting/setting.screen';
import { TransfersScreen } from '../features/transfers/transfers.screen';
import * as screenName from './screen-name'
import { BottomTabLogin } from './tab-navigator';
import { BottomTabMain } from './tab-navigator-main';
const mainStack=createStackNavigator({
  [screenName.LoginMapScreen]: LoginMapScreen,
  [screenName.LoginPhoneScreen]: LoginPhoneScreen,
  [screenName.LoginScreen]: LoginScreen,
  [screenName.HomeScreen]: HomeScreen,
  [screenName.SettingScreen]: SettingScreen,
  [screenName.TransfersScreen]: TransfersScreen,
  [screenName.BottomTabLogin]: BottomTabLogin,
  [screenName.BottomTabMain]: BottomTabMain,
},
{
  // initialRouteName: screenName.BottomTabLogin,
  initialRouteName: screenName.BottomTabMain,
  mode:'card',
  headerMode:'none',
}
)

const MainNavigation = createAppContainer(mainStack);
export default MainNavigation;