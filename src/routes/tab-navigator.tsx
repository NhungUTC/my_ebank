// import 'react-native-gesture-handler';
// import * as React from 'react';
// import { Text, View } from 'react-native';
// import { createStackNavigator } from '@react-navigation/stack';
// import { NavigationContainer } from '@react-navigation/native';
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
// import { LoginScreen } from '../features/login/login-screen';


// function LeftScreen() {
//   return (
//     <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//       <Text>Map</Text>
//     </View>
//   );
// }

// function CenterScreen() {
//   return (
//     <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//       <Text>Center</Text>
//     </View>
//   );
// }
// function RightScreen() {
//   return (
//     <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
//       <Text>Phone</Text>
//     </View>
//   );
// }

// const Tab = createBottomTabNavigator();
// const Stack = createStackNavigator();
// export const TabNavigatorComponent=()=>{
//   return (
//     // <NavigationContainer>

//       <Tab.Navigator>
//         <Tab.Screen name="Left" component={LeftScreen} />
//         <Tab.Screen name="Center" component={LoginScreen} />
//         <Tab.Screen name="Right" component={RightScreen} />
//       </Tab.Navigator>
//     // </NavigationContainer>
//   );
// }

import 'react-native-gesture-handler';
import * as React from 'react';
import { Image, Text, View, StyleSheet } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';
import { NavigationContainer } from '@react-navigation/native';
// import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { LoginScreen } from '../features/login/login-screen';

import * as screenName from './screen-name'
import { LoginMapScreen } from '../features/login-map/login-map.screen';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import images from '../res/images';
import { LoginPhoneScreen } from '../features/login-phone/login-phone.screen';
import colors from '../res/colors';
export const BottomTabLogin = createMaterialBottomTabNavigator(
  {
    //1
    [screenName.LoginMapScreen]: {
      screen: LoginMapScreen,
      navigationOptions: ({ navigation }) => ({
        title: "",
        tabBarIcon: ({ focused }) => {
          const img = focused ? images.ic_map : images.ic_map
          return (
            <View style={styles.viewIcon}>
              <Image style={styles.icon} source={img}></Image>
            </View>
          )
        },
      }),
    },
    //2
    [screenName.LoginScreen]: {
      screen: LoginScreen,
      navigationOptions: ({ navigation }) => ({
        title: "",
        tabBarIcon: ({ focused }) => {
          const img = focused ? images.ic_qrcode : images.ic_qrcode
          return (
            <View style={styles.viewIcon}>
              <Image style={styles.icon} source={img}></Image>
            </View>
          )
        },
      }),
    },
    //3
    [screenName.LoginPhoneScreen]: {
      screen: LoginPhoneScreen,
      navigationOptions: ({ navigation }) => ({
        title: "",
        tabBarIcon: ({ focused }) => {
          const img = focused ? images.ic_phone : images.ic_phone
          return (
            <View style={styles.viewIcon}>
              <Image style={styles.icon} source={img}></Image>
            </View>
          )
        },
      }),
    },
  },
  //init
  {
    initialRouteName: screenName.LoginScreen,
    activeColor: colors.primaryColor,
    inactiveColor: colors.black,
    barStyleLight: {
      height: 91,
      backgroundColor: colors.primaryColor,

    },
  }
);

const styles = StyleSheet.create({
  icon: {
    width: 24,
    height: 24
  },
  viewIcon: {
    width: 40,
    height: 40,
    // backgroundColor: 'pink',
    alignItems: 'center',
    justifyContent: 'center'
  },
  barBottom: {

  }
})
