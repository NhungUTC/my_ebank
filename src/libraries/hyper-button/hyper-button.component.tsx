import React from 'react'
import {View, StyleSheet, Image, TouchableOpacity, Text} from "react-native"
import {SvgXml} from 'react-native-svg'
// import images from '../../res/images';
import { HyperButtonProps } from './hyper-button.props';

export const HyperButtonComponent=(props: HyperButtonProps)=>{
    const{
     containerStyles,
     onPress,
     imgWidth=24,
     imgHeight=24,
     img={},
     imgStyle,
     text,
     textStyle,
    }=props;
    return(
      <TouchableOpacity
      style={[style.container, containerStyles &&{...containerStyles}]}
      onPress={onPress && onPress}
      >
          {text ?(
          <Text style={[style.textStyle, textStyle && {...textStyle}]} >{text}</Text>
          ):( //null
            <SvgXml
            width={imgWidth}
            height={imgHeight}
            xml={img}
            style={[style.imgStyle, imgStyle && {...imgStyle}]}
            >
            </SvgXml>
          )}
      </TouchableOpacity>
    );
}
const style=StyleSheet.create({
    container:{

    },
    imgStyle:{

    },
    textStyle:{
     
    }
})