export interface TextFieldState{
    value: string,
    showPass: boolean,
    boderInputColor: string,
    errorInput: boolean,
}