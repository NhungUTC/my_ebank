import {TextInputProps, TextStyle, ViewStyle} from "react-native"

export interface TextFieldProps extends TextInputProps{
    placeHolderText?: string,
    placeHolderColor?: string,
    inputStyle?: ViewStyle,
    containerStyle?: ViewStyle,
    containerInput?:ViewStyle,
    inputTextStyle?: TextStyle,
    onChangeValue?: (value: string) => void;
    lable?: string,
    iconLeft?:boolean,
    urlIconLeft?: URL,
    iconRight?:boolean,
    onPressBtnShowPass?:()=> void,
    passInput?: boolean;
    errorTxt?: boolean,
    errorInput?: boolean,
    defaultValue?: string,

}

