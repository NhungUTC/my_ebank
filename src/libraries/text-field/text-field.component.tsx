import React from 'react'
import { View, Text, TextInput, Platform, StyleSheet, TouchableOpacity, Image } from "react-native"
import images from '../../res/images';
import { HyperButtonComponent } from '../hyper-button/hyper-button.component';
import { TextFieldProps } from './text-field.props';
import { TextFieldState } from './text-field.state';

export const TextFieldComponent = (props: TextFieldProps, state: TextFieldState) => {
    const {
        placeHolderText,
        placeHolderColor,
        inputStyle,
        containerStyle,
        containerInput,
        inputTextStyle,
        onChangeValue,
        lable,
        iconLeft,
        urlIconLeft,
        iconRight,
        onPressBtnShowPass,
        passInput,
        errorTxt,
        errorInput,
        defaultValue,
    } = props;
    const [showPass, setShowPass] = React.useState(false);
    
    return (
        <View style={[styles.container, containerStyle]}>
            <View style={[styles.containerInput, containerInput]}>
                 {iconLeft ? (
                         <View style={styles.viewIconLeft}>
                         <View>
                             <Image
                                 style={styles.imgIcomLeft}
                                 source={urlIconLeft}></Image>
                         </View>
                     </View>
                ): null}
                <View style={[styles.wrapInput]}>
                   
                <TextInput
                        autoCapitalize="none"
                        style={[styles.input, inputStyle,inputTextStyle]}
                        placeholder={placeHolderText}
                        defaultValue={defaultValue}
                        placeholderTextColor={placeHolderColor}
                    > </TextInput>
                </View>
                {iconRight ? (
                    <HyperButtonComponent
                        containerStyles={{ padding: 4 }}
                        img={iconRight}
                        imgWidth={20}
                        imgHeight={20}
                    />
                ) : null}

                {passInput ? (
                    <HyperButtonComponent
                        containerStyles={{ padding: 4, }}
                        text={showPass ? "Ẩn" : "Hiện"}
                        textStyle={styles.textShowPass}
                        imgHeight={20}
                        imgWidth={20}
                    ></HyperButtonComponent>
                ) : null}
            </View>

        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        width: '100%',
    },
    wrapInput: {
        flex: 1,
        marginVertical: Platform.OS === 'ios' ? 10 : 0,
    },
    input: {
        color: "#FFFFFF",
        fontSize: 16,
        fontWeight: '400',

    },
    containerInput: {
        flexDirection: 'row',
        marginTop: 4,
        borderWidth: 1,
        borderColor: '#FFFFFF',
        height: 44,
        alignItems: 'center',
        paddingHorizontal: 5,
        borderRadius: 8,
        backgroundColor: 'rgba(255, 255, 255, 0.2)'
    },
    wrapLable: {
        width: '100%',
    },
    lable: {

    },
   
    viewIconLeft: {
        width: 30,
        height: 48,
        justifyContent: 'center',
        alignItems: 'center',
        // borderRadius: 8,
        // marginTop: 50,
        // backgroundColor: '#FF7F08'
    },
    imgIcomLeft: {
        width: 26,
        height: 26,

    },
    textShowPass:{
        fontSize: 14,
        fontWeight: '400',
        color: '#FFFFFF',
        textDecorationLine:'underline' 
    }

})