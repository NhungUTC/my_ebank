import {ViewStyle, TextStyle, ImageStyle}  from 'react-native'
export interface ActionBarProps{
    styleContainer?: ViewStyle,
    textStyle?: TextStyle,
    label?: string,
    //image
    isIconLeft?: boolean,
    isIconRight?: boolean,
    imgWidth?:number,
    imgHeight?: number,
    imgContainer?: ImageStyle,
    onPress?:()=> void,
    img?:string
}