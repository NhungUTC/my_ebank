import React from 'react'
import { View, Text, StyleSheet, Image, TouchableOpacity } from 'react-native'
import colors from '../../res/colors'
import { ActionBarProps } from './action-bar.props'

export const ActionBarCusComponent = (props: ActionBarProps) => {
    const {
        styleContainer,
        textStyle,
        label,
        isIconLeft,
        isIconRight,
        imgHeight = 24,
        imgWidth = 24,
        imgContainer,
        onPress,
        img,
    } = props
    return (
        <View style={[styles.container, styleContainer && { ...styleContainer }]}>
            {isIconLeft ? (
                <TouchableOpacity>
                    <Image source={img}

                        style={[styles.imgContainer, imgContainer && { ...imgContainer }]}
                    ></Image>
                </TouchableOpacity>

            ) : (null)}
            <View style={styles.viewText}>
                <Text
                    style={[styles.textStyle, textStyle && { ...textStyle }]}
                >{label}</Text>
            </View>

        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 48,
        backgroundColor: colors.primaryColor,
        paddingLeft: 6,
    },
    imgContainer: {
        marginLeft: 16
    },
    viewText: {
        flex:1,
        alignItems:'center',
        justifyContent:'center'
    },
    textStyle: {
        fontSize: 18,
        fontWeight: '500',
        lineHeight: 27,
        color: colors.white,
        // textAlign:'center'
    }
})

