import React from 'react'
import {View, Text, StyleSheet, Image} from "react-native"
import images from '../../res/images';
import { LabelProps } from './label.props';
export const LabelComponent=(props:LabelProps )=>{
    const {
     textStyle,
     textLabel,
     containerStyle,
     iconLeft,
    }=props;
    return(<View style={[styles.container , containerStyle && {...containerStyle}]}>
        <View>
            { iconLeft ?(
            <View>
                <Image style={styles.imgError}
                source={images.ic_error}
                ></Image>
            </View>
            ): null}
        </View>
        <Text style={[styles.textStyle, textStyle && {...textStyle}]}>{textLabel}</Text> 
    </View>);
}

const styles= StyleSheet.create({
    container:{
     flexDirection:'row'
        // flex: 1,
        //  justifyContent: 'center',
        // alignItems: 'center',
    },
    textStyle:{
        // fontSize: 14,
        // fontWeight: '500',
        // color: '#FFFFFF',
    },
    imgError:{
        width: 18,
        height: 18,
        marginLeft: 10,
        marginRight: 10
    }
})
