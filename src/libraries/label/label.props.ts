import {TextProps, ViewStyle, TextStyle} from 'react-native'
export interface LabelProps extends TextProps{
    textLabel?: string,
    textStyle?: TextStyle,
    containerStyle?: ViewStyle,
    iconLeft?: boolean
}