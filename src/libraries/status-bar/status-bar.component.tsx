import React from 'react'
import {View, StatusBar} from 'react-native'
import colors from '../../res/colors'

export const StatusBarComponent =()=>{
    return(
        <View>
            <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#115DD3" translucent = {false}/>
        </View>
    )
}