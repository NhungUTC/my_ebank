const images={
    bg_login: require('../../assets/bgLogin.png'),
    logo_hyper :require('../../assets/logoHyper2x.png'),
    ic_lock: require('../../assets/lock.png'),
    ic_userLine: require('../../assets/userLine.png'),
    ic_faceId: require('../../assets/faceId.png'),
    ic_fingerPrint: require('../../assets/fingerPrint.png'),
    ic_map: require('../../assets/map.png'),
    ic_qrcode: require('../../assets/qrCode.png'),
    ic_phone:require('../../assets/phone_login.png'),
    ic_error: require('../../assets/iconError.png'),
    ic_home: require('../../assets/iconHome.png'),
    ic_setting: require('../../assets/iconSetting.png'),
    ic_settingActive: require('../../assets/iconSettingActive.png'),
    ic_transfersActive: require('../../assets/iconTransfersActive.png'),
    ic_transfers: require('../../assets/iconTransfers.png'),
    ic_homeActive: require('../../assets/iconHomeActive.png'),
    ic_ring: require('../../assets/iconRing.png'),
    ic_search: require('../../assets/iconSearch.png'),
    //icon Home center
    ic_Bill: require('../../assets/iconCenterHome/iconBill.png'),
    ic_send: require('../../assets/iconCenterHome/iconSend.png'),
    ic_phoneReCharge: require('../../assets/iconCenterHome/phoneRecharge.png'),
    ic_searchCenter: require('../../assets/iconCenterHome/iconSearch.png'),
    ic_transfersCenter: require('../../assets/iconCenterHome/iconTransfers.png'),
    ic_transfersCenter2: require('../../assets/iconCenterHome/iconTransfers2.png'),
    ic_loan: require('../../assets/iconCenterHome/iconLoan.png'),
    ic_gift: require('../../assets/iconCenterHome/iconGift.png'),
    ic_wallet: require('../../assets/iconCenterHome/iconWallet.png'),
    //icon transfers
    ic_left: require('../../assets/iconTransfersScreen/iconLeft.png'),

}
export default images;