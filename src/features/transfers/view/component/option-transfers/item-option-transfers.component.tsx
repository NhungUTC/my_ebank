import React from 'react'
import { View, Text, StyleSheet, Image, } from 'react-native'
import { SvgXml } from 'react-native-svg'
import colors from '../../../../../res/colors'
import { svg } from '../../../../../res/svg'
// import svgs from '../../../../../res/svgs'

export const ItemOptionTransfersComponent = () => {
    return (
        <View style={styles.container}>
            <View>
                <SvgXml style={styles.imgSvg} width={46} height={49} xml={svg.iconAction.ic_earth}></SvgXml>
            </View>
            <View>
                <SvgXml width={6} height={12} xml={svg.iconAction.icon_open}></SvgXml>
            </View>

        </View>
    )
}
const styles = StyleSheet.create({
    container: {
        height: 80,
        backgroundColor: colors.white,
        borderRadius: 8,
        justifyContent: 'center',
        alignContent: 'center',
        flexDirection: 'row',

    },
    imgSvg: {
        backgroundColor: 'pink'
    }
})
