import React from 'react'
import { View, StyleSheet, Text, StatusBar } from 'react-native'
import { ActionBarCusComponent } from '../../libraries/action-bar/action-bar.component';
import colors from '../../res/colors';
import images from '../../res/images';
import { ItemOptionTransfersComponent } from './view/component/option-transfers/item-option-transfers.component';
export const TransfersScreen = () => {
    return (
        <View style={styles.container} >
           <ActionBarCusComponent
             styleContainer={styles.containerStyle}
             imgContainer={styles.imgContainer}
            isIconLeft={true} img={images.ic_left} label="Chuyển tiền"></ActionBarCusComponent>
            <View style={styles.contentContainer}>
             <ItemOptionTransfersComponent></ItemOptionTransfersComponent>
            </View>
        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        // flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    containerStyle:{
        flexDirection:'row',
        alignItems:'center',

    },
    //action Bar
    viewActionBar:{
        height: 48,
        backgroundColor: colors.primaryColor
    },
    imgContainer:{
        width: 24,
        height: 24,
        // backgroundColor:'pink'
    },
    //content
    contentContainer:{
     paddingHorizontal:16,
     backgroundColor:'#EDF2F8',
     height: "100%"
    }
})