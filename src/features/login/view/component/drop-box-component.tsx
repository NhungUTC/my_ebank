import React, { Component } from 'react';
import { View, Picker } from 'react-native'
import { Dropdown } from "react-native-material-dropdown";

export const DropDownComponent = () => {

  let data = [{
    value: 'Banana',
  }, {
    value: 'Mango',
  }, {
    value: 'Pear',
  }];

  return (
    <Dropdown
      label='Favorite Fruit'
      data={data}
    ></Dropdown>

  );
}