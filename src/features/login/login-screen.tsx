import React from 'react'
import { View, Text, Image, StyleSheet, ImageBackground, TouchableOpacity, Picker , StatusBar} from "react-native"
import { Colors } from 'react-native/Libraries/NewAppScreen';
import { HyperButtonComponent } from '../../libraries/hyper-button/hyper-button.component';
import { LabelComponent } from '../../libraries/label/label.component';
import { StatusBarComponent } from '../../libraries/status-bar/status-bar.component';
import { TextFieldComponent } from '../../libraries/text-field/text-field.component';


import images from '../../res/images';
// import { DropDownComponent } from './view/component/drop-box-component';
// import { TabNavigationComponent } from '../root/view/tab-naviagtion/tab-navigation.component';

export const LoginScreen = ({naviagtion}) => {
    const [userName, setUserName] = React.useState("Tài khoản");
    const [password, setPassword] = React.useState("Mật khẩu");
    const [selectedValue, setSelectedValue] = React.useState("java");
    return (
        <View>
            {/* <StatusBar barStyle = "light-content" hidden = {false} backgroundColor = "#115DD3" translucent = {false}/> */
            <StatusBarComponent></StatusBarComponent>}
            <ImageBackground style={style.image_bg} source={images.bg_login}>
                <View style={style.container}>
                    {/* image-logo */}
                    <Image style={style.imageLogo}
                        source={images.logo_hyper}
                    ></Image>
                    {/* pick */}
                   {/* <DropDownComponent></DropDownComponent> */}
                    {/* text-input */}
                    {/*error user-pass  */}
                    <LabelComponent
                    iconLeft={true}
                    textLabel="Tài khoản hoặc mật khẩu không đúng"
                    textStyle={style.textError}
                    containerStyle={style.viewTextError}
                    ></LabelComponent>
                    <TextFieldComponent
                        urlIconLeft={images.ic_userLine}
                        iconLeft={true}
                        containerStyle={{ marginTop: 20 }}
                        placeHolderText="Tài khoản"
                        defaultValue={userName}
                        placeHolderColor="white"
                    ></TextFieldComponent>
                    <TextFieldComponent
                        iconLeft={true}
                        urlIconLeft={images.ic_lock}
                        containerStyle={{ marginTop: 20 }}
                        placeHolderText="Mật khẩu"
                        defaultValue={password}
                        passInput={true}
                        placeHolderColor="white"
                    ></TextFieldComponent>
        
                    {/* button-sign */}
                    <View style={style.viewBtn}>
                        <HyperButtonComponent
                            containerStyles={style.signIn}
                            text="Đăng nhập"
                            textStyle={style.textSign}
                            // onPress={()=> naviagtion.navigate("BottomTabMain")}
                        ></HyperButtonComponent>
                        <TouchableOpacity style={style.signFaceId}>
                            <View>
                                <Image
                                    style={style.imgSignFaceId}
                                    source={images.ic_faceId}></Image>
                            </View>
                        </TouchableOpacity>
                    </View>

                    {/* label */}
                    <View style={{ marginTop: 50 }}>
                        <LabelComponent
                            textLabel={"Quên mật khẩu"}
                            textStyle={style.textLabel}
                            containerStyle={style.viewTextLabel}
                        ></LabelComponent>
                        <LabelComponent
                            textLabel={"Đăng ký tài khoản bằng eKYC"}
                            textStyle={style.textLabel}
                            containerStyle={style.viewTextLabel}
                        ></LabelComponent>
                    </View>

                </View>

            </ImageBackground>

        </View>
    );
}

const style = StyleSheet.create({
    image_bg: {
        width: '100%',
        height: '100%',
    },
    container: {
        paddingHorizontal: 32,
        paddingTop: 68
    },
    imageLogo: {
        width: 125,
        height: 57,
    },
    signIn: {
        width: '80%',
        height: 48,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
        // marginTop: 50,
        backgroundColor: '#FF7F08'
    },
    signFaceId: {
        width: 48,
        height: 48,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 8,
        // marginTop: 50,
        backgroundColor: '#FF7F08'
    },
    textSign: {
        fontSize: 16,
        fontWeight: '500',
        color: '#FFFFFF'
    },
    imgSignFaceId: {
        width: 26,
        height: 26,

    },
    viewBtn: {
        flex: 1,
        flexDirection: 'row',
        width: '100%',
        justifyContent: 'space-between',
        marginTop: 32


    },
    textLabel: {
        fontSize: 14,
        fontWeight: '500',
        color: '#FFFFFF',
    },
    viewTextLabel: {
        justifyContent: 'center',
        alignItems: 'center',
        marginTop: 20,
        // // backgroundColor:'pink'
    },
    textError:{
        color:'#FF4D54',
        fontWeight: '400',
        fontSize: 14,
        
    },
    viewTextError:{
        width: '100%',
        height:36,
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        borderRadius: 8,
        // justifyContent:'center',
        alignItems:'center',
        marginTop: 19,
        
    },
    imgError:{

    }

})