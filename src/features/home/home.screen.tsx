import React from 'react'
import { View, StyleSheet, Text, Image } from 'react-native'
import colors from '../../res/colors';
import { ItemActionComponent } from './view/component/action/itemAction/itemAction.component';
import { ListActionCoponent } from './view/component/action/listAction.component';
import { InformationComponent } from './view/component/information/information.component';
import { InputFindComponent } from './view/component/input-search/input-find.component';
import { InputSearchComponent } from './view/component/input-search/input-search.component';
export const HomeScreen = () => {
    return (
        <View style={styles.container} >
            {/* top */}
            <View style={styles.viewTop}>
                <View style={styles.viewSearchContainer}>
                    <InputSearchComponent></InputSearchComponent>
                </View>
                {/* <InputFindComponent></InputFindComponent> */}
                <View style={styles.containerInfor}>
                    <InformationComponent></InformationComponent>
                </View>

            </View>
            {/* center */}
            <View style={styles.viewCenter}>
                <View style={styles.containerList}>
                    {/* <ItemActionComponent></ItemActionComponent> */}
                    <ListActionCoponent></ListActionCoponent>
                </View>
            </View>


        </View>
    );
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
    },
    //----------------------top
    viewTop: {
        // flex: 5,
        height: 190,
        backgroundColor: "#115DD3",
        paddingHorizontal: 15,
        alignItems: 'center',
        left: 0,
        right: 0,

    },
    //viewFindContainer
    viewSearchContainer: {
        marginTop:50,
        width: '100%',
        // height:60,
        // backgroundColor: 'green'

    },
    containerInfor: {
        width: '100%',
        height: 144,
        position: 'relative',
        top: 20,
        backgroundColor: colors.primaryColor,
        borderRadius: 8,
        paddingHorizontal: 20,
        paddingTop: 15,
    },

    //-----------------------------------center
    viewCenter: {
        // flex: 7,
        backgroundColor: '#E9F0F6',
        paddingHorizontal: 15,
        position: 'relative',
        alignItems: 'center',
        // justifyContent:'space-between'
    },
    containerList: {
        width: '100%',
        position: 'absolute',
        height: 400,
        borderRadius: 8,
        // backgroundColor: 'pink',
        top: 118,
    }

})