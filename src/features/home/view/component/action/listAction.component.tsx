import React, { useEffect, useState } from 'react'
import { View, Text, StyleSheet, Image, FlatList } from 'react-native'
import { dataListAction } from './dataListAction';
import { ItemAction } from './itemAction/itemAction';
import { ItemActionComponent } from './itemAction/itemAction.component';


export const ListActionCoponent = () => {
    //  const dataListFromServer=dataListAction;
    const [dataList, setDataList]= useState<ItemAction[]>([]);
    useEffect(()=>{
      setDataList([...dataList, ...dataListAction])
    },[])
    return (
        <View>
         <FlatList
         data={dataList}
         renderItem={({item, index})=>(
            <ItemActionComponent itemAction={item}></ItemActionComponent>
         )}
         //set columns
         columnWrapperStyle={style.row}
         numColumns={3}
         keyExtractor={(item,index)=> index.toString()}
         ></FlatList>
        </View>
    );
}

const style = StyleSheet.create({
    row: {
        flex: 1,
        justifyContent: "space-between"
    }
});