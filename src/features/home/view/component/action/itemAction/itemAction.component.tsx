import React from 'react'
import { Image, StyleSheet, View, Text, TouchableOpacity, Alert } from 'react-native'
import images from '../../../../../../res/images'
import { ItemActionProps } from './itemAction.props'
export const ItemActionComponent = (props: ItemActionProps) => {
   const {itemAction}= props;
  
const [counter, setCounter] = React.useState(0)
const handleIncreaseCounterPress = () => {
 setCounter(counter + 1)
 console.log("counter:"+setCounter(counter+1))
}
    return (
        // <View style={styles.container}>
        <TouchableOpacity 
        onPress={handleIncreaseCounterPress}
         style={styles.container}>
            <Image source={itemAction?.url} style={styles.image}></Image>
            <Text > {itemAction?.label}</Text>
        </TouchableOpacity>
        // </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: 106,
        height: 90,
        borderRadius: 8,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
        // margin: 5,
        marginBottom:20
    },
    image: {
        width: 32,
        height: 32,
        marginBottom: 9
    },
    text: {
        fontSize: 14,
        fontWeight: '400',
        lineHeight: 21,
    }
})
