import { ItemAction } from "./itemAction";

export interface ItemActionProps{
   itemAction?: ItemAction;
}