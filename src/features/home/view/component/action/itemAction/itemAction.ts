export interface ItemAction{
    id?: number,
    url?: string,
    label?: string,
}