import React from 'react'
import { View, StyleSheet, Image, Text } from 'react-native'
// import { LinearGradient } from 'react-native-svg'
import LinearGradient from 'react-native-linear-gradient';
import { LabelComponent } from '../../../../../libraries/label/label.component';
import colors from '../../../../../res/colors';
export const InformationComponent = () => {
    return (
        // <View style={styles.container}>
            <View>
                {/* <LinearGradient style={styles.container} colors={['rgba(17,93,211,1)', 'rgba(39,131,216,1)']}>
         </LinearGradient> */}
                <LabelComponent
                    textLabel="PHAM TUAN ANH"
                    textStyle={styles.textName}
                    containerStyle={styles.viewTxtName}
                ></LabelComponent>
                <LabelComponent
                    textLabel="8014 5756 4567"
                    textStyle={styles.textName}
                    containerStyle={styles.viewTxtNum}
                ></LabelComponent>
                <View style={styles.line}><Text>111</Text></View>
                <View style={styles.viewDetail}>
                    <LabelComponent
                        textLabel="10,000,000,000 VND"
                        textStyle={styles.textMoney}
                    // containerStyle={styles.viewTxtNum}
                    ></LabelComponent>
                </View>
            </View>

        // </View>
    )
}

const styles = StyleSheet.create({
    textName: {
        fontWeight: '700',
        fontSize: 16,
        color: colors.white,
        lineHeight: 26
    },
    viewTxtName: {
        // backgroundColor: 'pink',
        width: '100%',

    },
    viewTxtNum: {
        // backgroundColor: 'pink',
        width: '100%',
        marginTop: 2
    },
    line: {
        width: '100%',
        height: 1,
        backgroundColor: 'rgba(255,255,255,0.2)',
        marginTop: 20

    },
    //detail
    viewDetail: {
        // backgroundColor:'green',
        width: '100%',
        marginTop: 15,


    },
    textMoney: {
        fontWeight: '700',
        fontSize: 18,
        color: colors.white,
        lineHeight: 26
    }


})