import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { TextFieldComponent } from '../../../../../libraries/text-field/text-field.component'
import images from '../../../../../res/images'

export const InputSearchComponent = () => {
    return (
        <View style={styles.viewSearchContainer}>
            <TextFieldComponent
                containerStyle={styles.viewFind}
                containerInput={styles.inputContainer}
                iconLeft={true}
                urlIconLeft={images.ic_search}
                placeHolderText="Tìm kiếm dịch vụ"
                placeHolderColor="#B3B8C2"
                defaultValue="Tìm kiếm"
                inputTextStyle={styles.inputText}
            ></TextFieldComponent>
            <View style={styles.viewIconRing}>
                <Image source={images.ic_ring} style={styles.iconRing}></Image>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    //viewFindContainer
    viewSearchContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
    },

    //viewFind
    viewFind: {
        //  backgroundColor:'green',
        width: '85%',
        // height: 36,
        justifyContent: 'center',
        alignItems: 'center'
    },
    inputContainer: {
        width: '100%',
        paddingRight: 19,
        height: 40,
        backgroundColor: 'rgba(255, 255, 255, 0.9)'

    },
    inputText:{
   fontSize: 16,
   fontWeight:'400',
   color:"#B3B8C2"
    },
    viewIconRing: {
        width: 36,
        height: 36,
        // backgroundColor: 'pink',
        justifyContent: 'center',
        alignItems: 'center',
        
    },
    iconRing: {
        width: 30,
        height: 30,
    },
});