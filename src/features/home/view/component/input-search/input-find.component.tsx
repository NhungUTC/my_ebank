import React from 'react'
import { View, Text, StyleSheet, Image } from 'react-native'
import { HyperButtonComponent } from '../../../../../libraries/hyper-button/hyper-button.component'
import { TextFieldComponent } from '../../../../../libraries/text-field/text-field.component'
import colors from '../../../../../res/colors'
import images from '../../../../../res/images'

// const screenWidth = Dimensions.get('window').width;
export const InputFindComponent = () => {
    return (
        <View style={styles.container}>
        <View style={styles.searchBarStyle}>
          <View style={styles.wrapBtn}>
            <HyperButtonComponent 
            // onPress={this.props.goBack}
              />
          </View>
          <View style={styles.wrapInput}>
            <TextFieldComponent
              placeholder="Tim kiem"
              style={styles.searchBarInputStyle}
              placeholderTextColor="#99A0AD"
            //   onChangeText={this.onChangeTextDelayed}
            />
          </View>
        </View>
      </View>
    )
}

const styles = StyleSheet.create({
    container: { overflow: 'hidden', paddingBottom: 5 },
  searchBarStyle: {
    width: '100%',
    height: 56,
    backgroundColor: colors.white,
    // paddingHorizontal: 16,
    alignItems: 'center',
    flexDirection: 'row',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,
    elevation: 3,
  },
  searchBarInputStyle: {
    color: colors.txtColor,
    fontSize: 14,
  },
  wrapBtn: {
    width: 56,
    height: 56,
    justifyContent: 'center',
    alignItems: 'center',
  },
  wrapInput: {
    // width: screenWidth - 56 - 16,
    height: 56,
    justifyContent: 'center',
  },
});